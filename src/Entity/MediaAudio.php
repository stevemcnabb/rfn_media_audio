<?php

namespace Drupal\rfn_media_audio\Entity;

use Drupal\node\Entity\Node;

/**
 * Defines the media audio entity.
 *
 * @ingroup media_audio
 *
 * @ContentEntityType(
 *   id = "media_audio",
 *   label = @Translation("media_audio"),
 *   base_table = "media_audio",
 *   revision_table = "media_audio_revision",
 *   revision_data_table = "media_audio_field_revision",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "published" = "status",
 *     "owner" = "uid",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 * )
 */
class MediaAudio extends Node implements MediaAudioInterface {

  const COLLECTION_TYPE_ALBUM = 1;
  const COLLECTION_TYPE_PLAYLIST = 2;

  /**
   * Collections for this media_audio entity.
   */
  public function collections($types = [1, 2]) {

    // Type 1 = Album, Type 2 = Playlist.
    $collections = \Drupal::service('entity_type.manager')
      ->getStorage('node')->loadByProperties([
        'type' => 'collection',
        'field_collection_type' => $types,
        'field_media_items' => $this->id(),
      ]);

    return $collections;
  }

  /**
   * Get the streaming uri for this track.
   */
  public function getStreamingUri() {

    $uri = 'private://' . $this->get('field_media_streaming_uri')->value;

    $stream = \Drupal::service('stream_wrapper_manager')->getViaUri($uri);
    $public_uri = $stream->getExternalUrl();

    \Drupal::messenger()->addMessage("for track {$this->getTitle()} see uri {$public_uri}");
    return $public_uri;

  }

  /**
   * Add this media_audio entity to a collection.
   */
  public function addToCollection($collection_nid, $track_id = NULL) {
    // @todo Implement addToCollection() method.
  }

  /**
   * Remove this media_audio entity from a collection.
   */
  public function removeFromCollection($collection_nid) {
    // @todo Implement removeFromCollection() method.
  }

  /**
   * {@inheritdoc}
   */
  public function urlInfo($rel = 'canonical', array $options = []) {
  }

  /**
   * {@inheritdoc}
   */
  public function url($rel = 'canonical', $options = []) {
  }

  /**
   * {@inheritdoc}
   */
  public function link($text = NULL, $rel = 'canonical', array $options = []) {
  }

  /**
   * {@inheritdoc}
   */
  public function getRevisionAuthor() {
  }

  /**
   * {@inheritdoc}
   */
  public function setRevisionAuthorId($uid) {
  }

}
