<?php

namespace Drupal\rfn_media_audio\Entity;

use Drupal\node\NodeInterface;

/**
 * Artist interface.
 */
interface MediaAudioInterface extends NodeInterface {

  /**
   * Collections associated with this media audio entity.
   */
  public function collections($types = [1, 2]);

  /**
   * Add this media audio entity to a collection.
   */
  public function addToCollection($collection_nid, $track_id = NULL);

  /**
   * Remove this media audio entity from a collection.
   */
  public function removeFromCollection($collection_nid);

  /**
   * What is the streaming uri of this track?
   */
  public function getStreamingUri();

}
